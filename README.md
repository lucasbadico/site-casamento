# Site casamento - Amanda e Lucas

##  Milestone 1 - Tasks:
 
 1. [x] - tema
 1. [x] - comprar dominio (lucaseamanda | amandaelucas) .online
 1. [x] - setup projeto
 1. [ ] - RSP - com fotos, historia e declaração de amor
 1. [ ] - ambiente prod
 1. [ ] - ambientes:
    - staging
    - prod
 1. [ ] - esteira devops (gitlab)
       

## Estrutura
There are 3 greater abstractions:
 - applicantions
 - components
 - packages

### applications
Instances that will produce output, api, sites, workers.

#### static-site
A base for the project as in html and css.

#### spa
The spa version of site, that can be personalized.

#### authentication
The authentication app. Integrated with google, facebook.

### authorization
The authorization functions baseds on tokens returned by authentication.

### packages
Sharable resources. Interface based ones.

### components
Sharable components. 